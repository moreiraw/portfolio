import React from 'react'
import Footer from '../components/footer'
import Icon from '../components/Icons'

/*..footer container file with the footer layout ready to be export to app.js.. */

export function FooterContainer(){
    return(
        <Footer>
            <Footer.Wrapper>
                <Footer.Row>
                <Footer.Column>
                    <Footer.Title>Contact </Footer.Title>
                    <Footer.Text>Email:</Footer.Text>
                    <Footer.Link href="#">wellington-cm@hotmail.com</Footer.Link>
                    <Footer.Text>Phone:</Footer.Text>
                    <Footer.Link href="#">07584494654</Footer.Link>
                </Footer.Column>

              
                <Footer.Column>
                    <Footer.Title>Find Me:</Footer.Title>
                    <Footer.Link href="https://www.linkedin.com/in/wellington-moreira-466a111a8/" target="_blank"><Icon className="fab fa-linkedin"/></Footer.Link>  
                    <Footer.Link href="https://bitbucket.org/moreiraw/portfolio/src/master/" target="_blank"><Icon className="fab fa-github"/>
                    </Footer.Link>  <Footer.Link href="#"><Icon className="fab fa-twitter"/></Footer.Link>
                </Footer.Column>

                <Footer.Column>
                    <Footer.Text>&copy;Copyright: WM Portfolio</Footer.Text>
                </Footer.Column>
                </Footer.Row>
            </Footer.Wrapper>
        </Footer>
    )
}

