import './App.css';
import Home from './components/Pages/Home'
import About from './components/Pages/About'
import Portfolio from './components/Pages/Portfolio';
import Contact from './components/Pages/Contact';
import Navbar from './components/Navbar/Navbar';
//import { BrowserRouter  as Routerct-router-dom';

/*..app.js component responsivel to hold all pages components of the portfolio plus navbar component on top..*/ 

function App() {
  return (
    <div className="App">
        <Navbar/>
      <Home/>
        <About/>
        <Portfolio/>
        <Contact/>
             
    </div>
  );
}

export default App;    //export default app.js component to the index.js component..

