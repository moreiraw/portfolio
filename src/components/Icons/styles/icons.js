import styled from 'styled-components'

/*social media icons with import styled components one of react dependencies */

export const Icon = styled.i`
    font-size: 30px;
    margin-right: 16px;
    border: 1px solid #fff;
    padding: 6px;

  
`