import React from "react";
import { Icon } from'./styles/icons'

/*icon component for the social media icons */

export default function Icons({className}){ /*export the icons for the pages portfolio website */

    return<Icon className={className} />
}