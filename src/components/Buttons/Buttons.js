import React from "react";
import './Buttons.css';

/*button component for website production */

const STYLES =[
    "btn--primary--outline",
    "btn--secondary--solid",
];

const SIZES = ["btn--large", "btn--small"];

export const  Button = ({   /*export the variable with function button follow with  parameters below */

    children, type, onClick,
    buttonStyle, 
    buttonSize
}) => {

    const checkButtonStyle = STYLES.includes(buttonStyle)
     ? buttonStyle : STYLES[0];

     const checkButtonSize = SIZES.includes(buttonSize) 
     ? buttonSize : SIZES[0];

    return(
       <button className={`btn ${checkButtonStyle} ${checkButtonSize}`}
             onClick={onClick} type={type}>
                {children}
        </button>
    )
};

