import React from "react";
import { Container, Wrapper, Row, Column, Link, Title, Text } from "./styles/footer";

/*..Index.js file to hold the component parts of the footer.. */

export default function Footer({children, ...restProps}){
    return <Container {...restProps}>{children}</Container>

}

Footer.Wrapper = function FooterWrapper({children,...restProps}){
    return <Wrapper className="overlay" {...restProps}>{children}</Wrapper>
}

Footer.Row = function FooterWrapper({children,...restProps}){
    return <Row {...restProps}>{children}</Row>
}

Footer.Column = function FooterWrapper({children,...restProps}){
    return <Column {...restProps}>{children}</Column>
}

Footer.Link = function FooterWrapper({children,...restProps}){
    return <Link {...restProps}>{children}</Link>
}

Footer.Title = function FooterWrapper({children,...restProps}){
    return <Title {...restProps}>{children}</Title>
}
Footer.Text = function FooterWrapper({children,...restProps}){
    return <Text {...restProps}>{children}</Text>
}