import styled from 'styled-components'

/*..footer style file using react style components..*/ 

export const Container = styled.div`
    padding:20px;
    background-color:#000000;
    margin-left:0px;
    width: 100vw;


`

export const Wrapper = styled.div`
display: flex;
flex-direction: column;
justify-content: start;
max-width: 100vw;
margin: 0 auto;    

`

export const Column = styled.div`
display: flex;
flex-direction: column;
text-align:left;
margin-left:10px;
margin-top: 10px;

`


export const Row = styled.div`
    display:grid;
    grid-template-columns: repeat(auto-fill, minmax(680px, 2fr));
    grid-gap: 10px;
   
   
`

export const Link = styled.a`
    display:inline-block;
    color: white;
    margin-bottom:10px;
    font-size: 15px;
    text-decoration: none;

    &:hover{
        color: gray;
        transition: 200ms ease-in;
    }

`

export const Title = styled.div`
   font-size: 16px;
   color: white;
   margin-bottom: 10px;
   font-weight: bold;
`
export const Text = styled.div`
    font-size: 14px;
    color: #fff;

`