import React from "react";
import './styles/contactContent.css';
import {Button} from './Buttons/Buttons'
import { useState } from "react";  /*import useState to use on the form input tags*/

/*contact content component to handle all the main content from the page */

function ContactContent(){

    const [name, setName, email, setEmail, subject, setSubject ] = useState(" ");  /*variable to hold array values to be used on usestate function */

    return(
    <main className="cContainer" > 

                <h1 className="title-c">Contact Me</h1>
                <article className="cContent">

                    <form>   {/*form tag to settle the inputs with referent array value..*/}
                        <label >
                            <span className="required">Full Name:</span>
                            <input type="text" name={name} onChange={(e) =>
                                 setName(e.target.value)}/>
                        </label>

                        <label>
                        <span className="required">Email:</span>
                            <input type="Email" name={email} onChange={(e) =>
                                 setEmail(e.target.value)}/>
                        
                        <span className="required">subject:</span>
                            <input type="text" name={subject} onChange={(e) =>
                                 setSubject(e.target.value)}/>
                        </label>

                        <label>
                        <span className="required">Message:</span>
                            <textarea/>

                        </label>

                    </form>
                       <section className="btContact">         
                            <Button   onClick={()=> {console.log("You clicked on me!")}}
                            type="button" buttonStyle="btn--secondary--solid"
                            buttonSize ="btn--small"
                            >Submit</Button>               { /*button component used for production  been added in the page content..*/}
                        </section>          

                    <section className="c-right">
                        <h4>Email:</h4>
                        <p>wellington-cm@hotmail.com</p>

                        <h4>Phone:</h4>
                        <p>07584494654</p>
                    </section>
               </article>
        

    </main>

    );

};

export default ContactContent;  /*export default contact content component to the app.js component.. */