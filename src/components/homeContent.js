import React from "react";
import './styles/homeContent.css';
import { HashLink as Link } from 'react-router-hash-link'; 
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faTwitter, faGithub, faLinkedin} from '@fortawesome/free-brands-svg-icons';
import { Button } from './Buttons/Buttons'

/*Home content component to handle all the main content from the page */

function HomeContent(){
    return(
    <section className="h-container" >
       
      <h2>Hi,</h2>
      <p>I am Wellington C Moreira
        Front-End Web Design</p>
        <a smooth="true" href="https://www.linkedin.com/in/wellington-moreira-466a111a8/" target="_blank"><FontAwesomeIcon className="icons" icon={faLinkedin} /></a>       
        <a smooth="true" href="https://bitbucket.org/moreiraw/portfolio/src/master/" target="_blank"><FontAwesomeIcon className="icons" icon={faGithub} /></a>       
        <a smooth="true" href="#" target="_blank"><FontAwesomeIcon className="icons" icon={faTwitter} /></a>

        <Link smooth="true" className="btOne" to="#portfolio"><Button className="btOne" onClick={()=> {console.log("You clicked on me!")}}
                    type="button" buttonStyle="btn--primary--outline"
                    buttonSize ="btn--large"
                    >View Portfolio</Button>       { /*button component used for production  been added in the page content..*/}
        </Link> 
    </section>

    );

  };

export default HomeContent;    /*export default home content component to the app.js component.. */