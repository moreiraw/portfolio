import React, { Component } from "react";
import { MenuItems } from "./MenuItems"
import './Navbar.css'
import logo from '../Images/logo.svg';
//import { Link } from 'react-router-dom';
import { HashLink as Link } from 'react-router-hash-link';  /*use react dependencies of hashlink router to be able to scroll to different pages than browser over the links */
//import {Link } from 'react-scroll';
 

/*navbar class component with react render */

class Navbar extends Component {
    state = { clicked: false }  /*setle the menu click states to false to no show the X close when menu bars is visible */

    handleClick = () => {
        this.setState({clicked: !this.state.clicked })   /*handleClick function to handle user click to open and close the menu icon bars with the screens under 960px */

    }

 
    render() {
    
        return(
            <nav className="NavbarItems Active">
                
                <Link smooth to="#home" className="link logn"><img src={logo} alt="wm portfolio logo"/></Link>
                <Link smooth to="#home" className="link" ><h1 className="navbar-logo">Portfolio</h1></Link>   
                    
                <section className="menu-icon" onClick={this.handleClick}>     { /*handleClick been used in the section  */}

                    <i className={this.state.clicked ? 'fas fa-times' : 'fas fa-bars'}></i>
                </section>

                <ul className={this.state.clicked ? 'nav-menu active' : 'nav-menu'}>
                    {MenuItems.map((item, index) => {
                         return (
                            <li key={index}>
                                <Link smooth className={item.cName} to={item.Link}>     
                                    {item.title}   
                                </Link>
                            </li>

                         )   
                    })
                    }

                </ul>

            </nav>
        );
    }
}

export default Navbar   /*export default navbar component to the app.js component */
