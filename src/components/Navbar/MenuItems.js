
/*navigation bar menu items components export as par keys and values to be use in the nav links  */

export const MenuItems = [
    {
        title: 'Home',
        Link: '#home',
        cName: 'nav-links'
    },
    {
        title: 'About ',
        Link: '#about',
        cName: 'nav-links'
    },
    {
        title: 'Portfolio',
        Link: '#portfolio',
        cName: 'nav-links'
    },
    {
        title: 'Contact',
        Link: '#contact',
        cName: 'nav-links'
    }

  

 
 
]