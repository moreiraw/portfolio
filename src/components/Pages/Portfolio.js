import React from "react";
import PortfolioContent from "../portfolioContent";

/*portfolio.js component to handle portfolio content components */
function Portfolio() {
    return(

        <section className="content" id="portfolio">
              <PortfolioContent/>  {/*portfolio content component been used */}
        </section>
    );
}

export default Portfolio;   /*export portfolio.js component to the app.js */