import React from "react";
import HomeContent from "../homeContent";

/*home.js component to handle the home content components */
function Home() {
    return(

        <section className="content"  id="home">

             <HomeContent />  {/*home content  component been used */}
        </section>
    );
}

export default Home;  /*export home.js component to the app.js */