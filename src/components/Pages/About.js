import React from "react";
import AboutContent from "../aboutContent";

/*about.js component to handle the about content pages */

function About() {   /*about function to set the about content component imported */
    return(

        <section className="content" id="about">
             <AboutContent/>   {/*about content components been used */}

        </section>
    );
}

export default About;  /*export the about.js component to the app.js component.. */
