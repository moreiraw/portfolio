import React from "react";
import ContactContent from "../contactContent";
import  { FooterContainer }  from "../../containers/footer";   /*import footer component to the last page of the website contact page..*/

/*contact.js component to handle the contact js and footer components */

function Contact() {
    return(

        <section className="content" id="contact">
                <ContactContent/>    {/*contact content component been used */}
                <FooterContainer/>    {/*footer component been used */}

        </section>
    );
}

export default Contact;  /*export contact.js component to the app.js */