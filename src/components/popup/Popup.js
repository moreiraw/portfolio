import React from "react";
import "./Popup.css"


/*popup component responsive to open a pop up screen when the user clicks in the portfolio immages wireframes.. */
const Popup = props => {
    return(
        <section className="popup-box">
            <figure className="box">
                <button className="btn-close" onClick={props.handleClose }>X</button>
                { props.content }
            </figure>
        </section>

                
    )
}

export default Popup;  /*export default of popup component to the portfolio content component */
