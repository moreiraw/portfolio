
import React from "react";
import './styles/aboutContent.css';
import Photo from './Images/myself.jpg';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faTwitter, faGithub, faLinkedin} from '@fortawesome/free-brands-svg-icons';  /*import fontawesome to handle the social media icons in the page..*/

/*about content component to handle all the main content from the page */

function AboutContent(){
    return(
    <main className="a-container">  {/*main tag to hold all the parts of the content */}


                  <h1 className="title-a">About Me</h1>
                  <section className="a-content">

                        <section className="sec1">
                            <img className="photo" src={Photo} alt="wellington photo" />
                            <section className="txt">
                                <p>Hi!</p>
                                    <p>My name is Wellington Carlos Moreira, I am a Brazilian who is living in the UK for over 10 years.
                                    I am an artist with great skills in graphic design with more than 15 years of experience, and since 2019 I have been studying Web design and development at Edge Hill University. I am now in my last third year to graduate as a web designer. Combining my graphic design skills in the web industry has been open to me a new field of knowledge and amplified my passion and desire to mix designs with cut edge technology.  
                                    The main purpose of this portfolio website is to provide a some of the work that I have done  in Web design and development, during my years of studying inside and outside the university,  Those jobs will demonstrate the web skills that I have learned throughout those years of learning.
                                    Please check my portfolio page for the jobs.
                                    If you would like to find out more, please get in touch with me, on my contact page or my social media.
                                </p>

                                <ul>
                                <li>Name: Wellington C Moreira</li>
                                <li>age: 42 years old</li>
                                <li>Ocupation: Student of Web Design and Development</li>
                                </ul>
                            
                                 {/*icons links below assign the a tag */}

                                <a smooth="true" href="https://www.linkedin.com/in/wellington-moreira-466a111a8/" target="_blank">
                                    <FontAwesomeIcon className="icons-a" icon={faLinkedin} /></a>       
                                <a smooth="true" href="https://bitbucket.org/moreiraw/portfolio/src/master/" target="_blank">
                                    <FontAwesomeIcon className="icons-a" icon={faGithub} /></a>       
                                <a smooth="true" href="#" target="_blank"><FontAwesomeIcon className="icons-a" icon={faTwitter} /></a>
                            </section>

                        </section>

                        <section className="sec2">                                     {/*second section of the page  */}
                            <h3>Skills</h3> 
                            <section className="sec2-sk">
                                <ul className="skl1">
                                    <li>Illustrator.................................95%</li>
                                    <li>Photoshop...............80%</li>
                                    <li>Html5 .................................90%</li>
                                    <li>CSS3..............................85%</li>
                                    <li>JavaScript..................60%</li>
                                    <li>SASS...................35%</li>
                                    <li>React........................50%</li>
                                    <li>NodeJs................40%</li>
                                    <li>Figma..........................75%</li>
                                </ul>

                                <ul className="skl1">
                                    <li>Graphic design..............................95%</li>
                                    <li>UX design.......................80%</li>
                                    <li>UI design.............................85%</li>
                                    <li>JQuery............40%</li>
                                    <li>PHP.............35%</li>
                                    <li>Python.............40%</li>
                                    <li>Bootstrap.......35%</li>
                                    <li>Mysql...........................65%</li>
                                    <li>Laravel............40%</li>
                                </ul>

                                <ul className="skl1 skll">
                                    <li>Team player.........................90%</li>
                                    <li>Problem solving.........65%</li>
                                    <li>Comunication skills....50%</li>
                                    <li>Creative mind...............................95%</li>
                                    <li>Deadline driven........................95%</li>
                                    <li>Proactive...................................95%</li>
                                    <li>Ethical work................................95%</li>
                                    <li>Positive attitude...........................95%</li>
                                    <li>Hard work person........................95%</li>
                
                                </ul>
                            </section>
                        </section>
                  </section>

    </main>              /*End of main tag content*/


    );

};

export default AboutContent;        /*export default about content to the content.js component*/

