//1st part portfolio content imports
import React, {useState } from "react";
import './styles/portfolioContent.css';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faTwitter, faGithub, faLinkedin} from '@fortawesome/free-brands-svg-icons';
import ColourS from './Images/colourS.svg';
import Typography from './Images/typography.svg';
import Sketches from "./Images/sketches.png";
import LowWire from "./Images/lowWire.png";
import ProtoType from "./Images/tabP.png";
import ProtoTypes from "./Images/proto1.png";
import ProtoType1 from "./Images/deskP.png";
import ProtoType2 from "./Images/mobP.png";

//2nd part portfolio content imports
import ColourTree from "./Images/colourT.svg";
import SideMap from "./Images/s-map.png";
import TypographyTree from "./Images/typoT.svg";
import B2Sketches from "./Images/brief2Sket.png";
import B2Low from "./Images/brief2Low.png";
import B2Tab from "./Images/b2Tab.png";
import B2Desk from "./Images/b2Desk.png";
import B2Mob from "./Images/b2Mob.png";
import B2Prototype from "./Images/b2Prot.png";
import B2About from "./Images/b2About.png";
import B2Contact from "./Images/b2Contact.png";
import B2Services from "./Images/b2Services.png";

//imports for components popup and button for the whole portfolio page.
import {Button} from './Buttons/Buttons';
import Popup from "./popup/Popup";


/*POrtfolio content component to handle all the main content from the page */

function PortfolioContent(){

    const [isOpen, setIsOpen] = useState(false);  //Setttle the pop up false so doesn show on the screen beofre user clicks.

    const togglePopup = () => {   //variable togglePopup to hold the popup function from 1st part wireframes responsivel to open the pop up after click

        setIsOpen(!isOpen);
    };

    const toggleIt = () => {    //variable toggleit to hold the popup fucntion from 2nd part wireframes responsivel to open the pop up after click
        setIsOpen(!isOpen);
    };

    return(

    <main className="p-container">
                 <section className="intro">   
                    <h1 className="title-p">My Portfolio</h1>

                    <p>My portfolio page will show a couple of my university works, demonstrating some of my skills in designing and coding.
                        Those projects were created using web design tools and followed their requirements and make used of  a creative mind
                        to design the page layout, they are calling Brief tasks and consisted of a small task brief 1 with just one single page
                        and a large task brief  2 from a real client consisting of a complete website with few basic pages required by the client.
                    </p>

                </section>

                    <article className="pt1">
                        <section className="pt1-1">
                            <h2>Brief Nº1</h2>
                            <p>
                                The brief nº 1 small task, was selected for the creation of a
                                single page website of a chosen object in the house and the webpage
                                is to sell this object. The object chosen was a handmade cigar box guitar. 
                                The design content from text to images and layout was a unique creation.
                                The creation followed the basic requirements specified by the module.
                            </p>

                              <aside className="ic">  
                                    <a smooth="true" href="https://www.linkedin.com/in/wellington-moreira-466a111a8/" target="_blank">
                                        <FontAwesomeIcon className="icons-a" icon={faLinkedin} /></a>       
                                    <a smooth="true" href="https://bitbucket.org/moreiraw/portfolio/src/master/" target="_blank">
                                        <FontAwesomeIcon className="icons-a" icon={faGithub} /></a>       
                                    <a smooth="true" href="#" target="_blank"><FontAwesomeIcon className="icons-a" icon={faTwitter} /></a>
                              </aside>  

                              <hr></hr>
                            
                        </section>

                        <section className="pt1-2">
                            <h3>Project management:</h3>

                            <p>
                            The first stage of any project complex or not is the breakdown of the same in small easy 
                              to manage tasks, as part of a project management this tasks are divided in to phases which
                              their respective importance in the project, the phase 1 is responsivel for the requirement
                              phases of the project in this project is followed the module  requirement for the brief, 
                              phase 2 hold the ideation or brainstorm  part, layout colour scheme and so on, phase 3 is 
                              the design of the ideas with rough sketches and wireframes and phase 4  based on the design the
                              code construction of the project, following the basic requirements and at end a testing task of the system. 

                            </p>
                            
                            <section className="pt1-block">
                                <section className="pt1-block1">
                                    <h4>Phase 1</h4>
                                    <aside className="phase1">
                                        <strong>Requirements:</strong>
                                        <ul>
                                            <li>Version control to store code in the repository-git</li>
                                            <li>Comment and well formated code.</li>
                                            <li>semantic html5</li>
                                            <li>Css Grid</li>
                                            <li>SASS or LESS</li>
                                            <li>Responsive layout mobile, tablet and desktop</li>
                                            <li>Browser support latest versions, chrome, firefox, IE8.</li>
                                        </ul>
                                    </aside>
                                </section>

                                <section className="pt1-block1">
                                    <h4>Phase 2</h4>
                                    <aside className="phase2">
                                        <strong>Ideation:</strong>
                                        <ul>
                                            <li>Find the right object to be used from the house</li>
                                            <li>Take pictures from different angles of the object.</li>
                                            <li>Brainstorm the layout ideas</li>
                                            <li>Thinking of colour scheme</li>
                                            <li>Typography style to be used</li>
                                            
                                        </ul>
                                    </aside>
                                </section>

                                <section className="pt1-block1">
                                    <h4>Phase 3</h4>
                                    <aside className="phase3">
                                        <strong>Design:</strong>
                                        <ul>
                                            <li>Rough sketches of the page.</li>
                                            <li>Figma low fidelity wireframe.</li>
                                            <li>figma High fidelity wireframe</li>
                                            
                                        </ul>
                                    </aside>
                                </section>

                                <section className="pt1-block1">
                                    <h4>Phase 4</h4>
                                    <aside className="phase4">
                                        <strong>Code implementation:</strong>
                                        <ul>
                                            <li>Create the project repository</li>
                                            <li>Code the Html5</li>
                                            <li>Code Css Grid</li>
                                            <li>SASS compile (SCSS)</li>
                                            <li>Code JavaScript</li>
                                            <li>Add JQuery plugings</li>
                                            <li>Media resaponsive screen sizes</li>
                                            <li>Testing functionality and usability</li>
                                        </ul>
                                    </aside>
                                </section>

                                <hr></hr>

                            </section>

                        </section>

                        <section className="pt1-3">
                                <h3>Project Design:</h3>

                                <p>
                                    With  the project phases organized the next step is the start point from 
                                    phase 2 ideation to phase 3 design, where the ideas for colour scheme and 
                                    typography come in mind and , for this project was used the Figma tool to help 
                                    to separeted and classify to colour tons and order that will be used in the project,
                                    as well as the type of typography with better suits the design aspect. 
                                </p>

                                <section className="pt1-block2">

                                    <section className="pt1-3Title colour">
                                        <h4>Colour Scheme Used:</h4>
                                        <img src={ColourS} alt="colour scheme pallet" />
                                    </section>

                                    <section className="pt1-3Title">
                                        <h4>Typography Used:</h4>
                                        <img src={Typography} alt="typography scheme pallet used" />
                                    </section>
                                </section>

                                <hr></hr>

                        </section>

                        <section className="pt1-4">
                            <h3>Project Design Sketches and Wireframe:</h3>

                            <p>
                                
                                After the colour scheme and typograghy been finalized, the next step 
                                was make use of the creativity and start put any early design ideas in a 
                                paper as some rough sketches to visualize a possible layout to be used
                                 in the design of the pages, followed the next part was with the help of figma tool,
                                  develop a low fidelity wireframe of the sketches. At this point with the colours,
                                   typograpy  and sketches ready, the next is start the creation of high fidelity 
                                   prototype on figma, with will give the background for the development of the
                                    interface system, following the phase 4 implementation of the system with all 
                                    the code construction of the page, as the first part of the phase 4 is create a 
                                    repository for the system code construction which can be found by click on the button below, 
                                    view code to access the code repository. 
                            </p>

                            <section className="pt1-block3">
                                  
                                  <section className="sketches">
                                     <h4>Rough Sketches</h4>
                                     <img onClick={togglePopup} src={Sketches} alt="rough sketches of the page" /> 

                                      <h4>Figma Low Fidelity Wireframe</h4>
                                     <img onClick={togglePopup} src={LowWire} alt="low fidelity wireframe of the page" /> 
                                       
                                  </section>  

                                  <section className=" high">
                                     <h4>Figma High Fidelity Wireframe</h4>

                                     <section  onClick={togglePopup} className="highW">

                                            <img src={ProtoType}  className="tab" alt="high fidelity prototype" /> 
                                            <img src={ProtoType1} className="desk" alt="high fidelity prototype" /> 
                                            <img src={ProtoType2}  className="mob" alt="high fidelity prototype" /> 
                                     </section>
                                     
                                  </section>
                                    
                                    {/*..line of code for pop up with wireframe images..*/}     

                                   {isOpen && <Popup
                                       handleClose={ togglePopup } 
                                       content = {
                                           <figure>

                                               <h4>High Fidelity Prototype</h4>
                                               <img src={ProtoTypes} className="protoT Img" alt="high fidelity prototype" /> 

                                               <h4>Rough Sketches</h4>
                                                <img src={Sketches} className="Img" alt="rough sketches of the page" /> 
                                                
                                                <h4>Figma Low Fidelity Wireframe</h4>
                                                <img src={LowWire} className="Img" alt="low fidelity wireframe of the page" /> 

                                           </figure>
                                          }
                                       />}  
                                     {/*..End of pop up images code ..*/}     

                            </section>

                        </section>

                        <section className="btnP">

                            <a smooth="true" className="btnP1" href="https://bitbucket.org/carloswell/brief-task1-small-portfolio/src/master/" target="_blank">
                                <Button   onClick={()=> {console.log("You clicked on me!")}}
                                    type="button" buttonStyle="btn--secondary--solid"
                                    buttonSize ="btn--small"
                                    >View Code
                                </Button>
                            </a>

                        </section>

                    </article> {/*..End the 1st part of portfolio content..*/}     

                      {/*..Portfolio page 2nd part start below..*/}     

                    <article className="pt2">
                        
                        <section className="pt1-1 pt2-1">
                                <h2>Brief Nº2</h2>
                                <p>
                                    The brief nº 2 large task, was selected to create a new complete website for the 
                                    client Edge Tree surgery. The design layout and typography was a unique creation by the developer,
                                    the pictures and company logotype was provided by the client as well as the requirements 
                                    for each page's text and specifications. The creation followed the basic requirements 
                                    specified by the client, with some developer creativity.
                                </p>

                                <aside className="ic">  
                                        <a smooth="true" href="https://www.linkedin.com/in/wellington-moreira-466a111a8/" target="_blank">
                                            <FontAwesomeIcon className="icons-a" icon={faLinkedin} /></a>       
                                        <a smooth="true" href="https://bitbucket.org/moreiraw/portfolio/src/master/" target="_blank">
                                            <FontAwesomeIcon className="icons-a" icon={faGithub} /></a>       
                                        <a smooth="true" href="#" target="_blank"><FontAwesomeIcon className="icons-a" icon={faTwitter} /></a>
                                </aside>  

                                <hr></hr>
                                
                        </section>
                            
                        <section className="pt2-2 pt1-2">
                            <h3>Project management:</h3>

                            <p>
                              The first stage of any project complex or not is the breakdown of the same in small easy 
                              to manage tasks, as part of a project management this tasks are divided in to phases which
                              their respective importance in the project, the phase 1 is responsivel for the requirement
                              phases of the project in this project is followed the module  requirement for the brief, 
                              phase 2 hold the ideation or brainstorm  part, layout colour scheme and so on, phase 3 is 
                              the design of the ideas with rough sketches and wireframes and phase 4  based on the design the
                              code construction of the project, following the basic requirements and at end a testing task of the system. 

                            </p>
                            
                            <section className="pt2-block pt1-block">

                                <section className="pt2-block1 pt1-block1">
                                    <h4>Phase 1</h4>
                                    <aside className="phase1">
                                        <strong>Requirements:</strong>
                                        <ul>
                                            <li>Header, footer, hero banner</li>
                                            <li>Good images usage</li>
                                            <li>Good CTAs</li>
                                            <li>Prominetn USPs</li>
                                            <li>Pages, home, about, contact and services </li>
                                            <li>Responsive layout mobile, tablet and desktop</li>
                                            <li>Add areas cover by the company</li>
                                        </ul>
                                    </aside>
                                </section>

                                <section className="pt2-block1 pt1-block1">
                                    <h4>Phase 2</h4>
                                    <aside className="phase2">
                                        <strong>Ideation:</strong>
                                        <ul>
                                            <li>Background reserach on different online tree surgery companies</li>
                                            <li>Organise the requirement ideas</li>
                                            <li>Create a side-map</li>
                                            <li>Thinking of colour scheme</li>
                                            <li>Typography style to be used</li>
                                            
                                        </ul>
                                    </aside>
                                </section>

                                <section className="pt2-block1 pt1-block1">
                                    <h4>Phase 3</h4>
                                    <aside className="phase3">
                                        <strong>Design:</strong>
                                        <ul>
                                            <li>Rough sketches of the page.</li>
                                            <li>Figma low fidelity wireframe.</li>
                                            <li>figma High fidelity wireframe</li>
                                            
                                        </ul>
                                    </aside>
                                </section>

                                <section className="pt2-block1 pt1-block1">
                                    <h4>Phase 4</h4>
                                    <aside className="phase4">
                                        <strong>Code implementation:</strong>
                                        <ul>
                                            <li>Create the project repository</li>
                                            <li>Create a React js project</li>
                                            <li>React components</li>
                                            <li>Page content components</li>
                                            <li>Html5 components</li>
                                            <li>Css and Csss Grid components</li>
                                            <li>Media resaponsive screen sizes</li>
                                            <li>Testing functionality and usability</li>
                                        </ul>
                                    </aside>
                                </section>

                                <hr></hr>

                            </section>

                        </section>

                        <section className="pt2-3 pt1-3">
                                <h3>Project Design:</h3>

                                <p>
                                    With  the project phases organized the next step is the start point from phase
                                    2 ideation to phase 3 design, where the ideas for colour scheme and typography 
                                    come in mind and , for this project was used the Figma tool to help to separeted
                                    and classify to colour tons and order that will be used in the project, and 
                                    the type of typography with better suits the design aspect.  The side-map create
                                    on Miro webiste to help understand the pages links and buttons. 
                                </p>

                                <section className="pt2-block2 pt1-block2">

                                    <section className="pt2-3Title colour pt1-3Title colour">
                                        <h4>Colour Scheme Used:</h4>
                                        <img src={ColourTree} alt="colour scheme pallet" />
                                    </section>

                                    <section className="pt2-3Title pt1-3Title">
                                        <h4>Side-Map:</h4>
                                        <img src={SideMap} className="sidemap" alt="typography scheme pallet used" />
                                    </section>

                                    <section className="pt2-3Title pt1-3Title">
                                        <h4>Typography Used:</h4>
                                        <img src={TypographyTree} alt="typography scheme pallet used" />
                                    </section>
                                </section>

                                <hr></hr>

                        </section>

                        <section className="pt1-4 pt2-4">

                            <h3>Project Design Sketches and Wireframe:</h3>

                            <p>
                                
                                After the colour scheme and typograghy been finalized, the next step 
                                was make use of the creativity and start put any early design ideas in a 
                                paper as some rough sketches to visualize a possible layout to be used
                                 in the design of the pages, followed the next part was with the help of figma tool,
                                  develop a low fidelity wireframe of the sketches. At this point with the colours,
                                   typograpy  and sketches ready, the next is start the creation of high fidelity 
                                   prototype on figma, with will give the background for the development of the
                                    interface system, following the phase 4 implementation of the system with all 
                                    the code construction of the page, as the first part of the phase 4 is create a 
                                    repository for the system code construction which can be found by click on the button below, 
                                    view code to access the code repository. 
                            </p>

                            <section className="pt1-block3 pt2-block3">
                                  
                                  <section className="sketches sketchesPt2">
                                     <h4>Rough Sketches</h4>
                                     <img onClick={toggleIt} src={B2Sketches} className="b2sket" alt="rough sketches of the page" /> 

                                      <h4>Figma Low Fidelity Wireframe</h4>
                                     <img onClick={toggleIt} src={B2Low} className="b2Low" alt="low fidelity wireframe of the page" /> 
                                       
                                  </section>  

                                  <section className=" high">
                                     <h4>Figma High Fidelity Wireframe</h4>

                                     <section  onClick={toggleIt} className="highW">
                                            <img src={B2Tab}  className="tab" alt="high fidelity prototype" /> 
                                            <img src={B2Desk} className="desk deskPt2" alt="high fidelity prototype" /> 
                                            <img src={B2Mob}  className="mob" alt="high fidelity prototype" /> 
                                            
                                     </section>
                                     
                                  </section>
                                    
                                    {/*..line of code for pop up with wireframe images 2nd part..*/}     

                                   {isOpen && <Popup
                                       handleClose={ toggleIt } 
                                       content = {
                                           <figure>

                                               <h4>High Fidelity Prototype</h4>
                                               <img src={B2Prototype} className="protoT Img" alt="high fidelity prototype" /> 

                                               <h4>Rough Sketches</h4>
                                                <img src={B2Sketches} className="Img" alt="rough sketches of the page" /> 
                                                
                                                <h4>Figma Low Fidelity Wireframe</h4>
                                                <img src={B2Low} className="Img" alt="low fidelity wireframe of the page" /> 
                                                
                                                <img src={B2About}  className="desk" alt="high fidelity prototype" /> 
                                            
                                                <img src={B2Contact} className="desk" alt="high fidelity prototype" /> 
                                                
                                                <img src={B2Services}  className="desk" alt="high fidelity prototype" /> 
                                           </figure>
                                          }
                                       />}  
                                     {/*..End of pop up images code ..*/}     


                                    <section className=" highPages">

                                            <section  onClick={toggleIt} className="highWp">
                                                    <img src={B2About}  className="desk deskPt2" alt="high fidelity prototype" /> 
                                                    <img src={B2Contact} className="desk deskPt2" alt="high fidelity prototype" /> 
                                                    <img src={B2Services}  className="desk deskPt2" alt="high fidelity prototype" /> 
                                            </section>             
                                    </section> 

                        </section>

                     </section>   

                      <section className="btnP">

                            <a smooth="true" className="btnP1" href="https://bitbucket.org/carloswell/brief-task2-large/src/master/" target="_blank">
                                <Button   onClick={()=> {console.log("You clicked on me!")}}
                                    type="button" buttonStyle="btn--secondary--solid"
                                    buttonSize ="btn--small"
                                    >View Code
                                </Button>
                            </a>

                        </section>
               
                 </article>     {/*..End 2nd part portfolio content..*/}       
                       

    </main>

    );

};

export default PortfolioContent;     /*..export portfolio content component to app.js component..*/     