import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import {BrowserRouter as Router } from 'react-router-dom';   //import react router dom from dependencies..


ReactDOM.render(   //use render function to deal with all components and send to id root on index. html file template
    <Router>                        {/*..user router component to link the app and the pages together...*/ }
        <React.StrictMode>
            <App />
        </React.StrictMode>
    </Router>,
     document.getElementById('root')   //get  element by id root in index.html template page.
);

